package com.burakkutbay.freamework;

import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@SuppressWarnings("rawtypes")
public interface GenericDao {
    void save(BaseEntity entity);

    void delete(BaseEntity entity);

    void update(BaseEntity entity);

    List getAll(Class clazz);

    BaseEntity getById(Class clazz, Long id);

    SorguSonucu getAll(Class clazz, int first, int pageSize, String sortField,
                       SortOrder sortOrder, Map<String, Object> filters);
}
