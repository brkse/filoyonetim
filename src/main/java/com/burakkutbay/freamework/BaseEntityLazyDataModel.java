package com.burakkutbay.freamework;

import org.primefaces.model.LazyDataModel;

import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
public class BaseEntityLazyDataModel extends LazyDataModel<BaseEntity> {

    @Override
    public Object getRowKey(BaseEntity baseEntity) {
        return baseEntity.getId();
    }

    @Override
    public BaseEntity getRowData(String rowKey) {
        List<BaseEntity> list = (List<BaseEntity>) getWrappedData();
        for (BaseEntity entity : list) {
            if (entity.getId().toString().equals(rowKey)) {
                return entity;
            }
        }
        return null;
    }


}
