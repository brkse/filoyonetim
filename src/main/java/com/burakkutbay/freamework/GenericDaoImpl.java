package com.burakkutbay.freamework;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */

@SuppressWarnings("rawtypes")
@Repository("genericDao")
@Transactional
public class GenericDaoImpl implements GenericDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void save(BaseEntity entity) {
        sessionFactory.getCurrentSession().save(entity);
    }

    @Override
    public void delete(BaseEntity entity) {
        sessionFactory.getCurrentSession().delete(entity);
    }

    @Override
    public void update(BaseEntity entity) {
        try {
            sessionFactory.getCurrentSession().update(entity);
        } catch (Exception e) {
            sessionFactory.getCurrentSession().merge(entity);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List getAll(Class clazz) {
        return sessionFactory.getCurrentSession()
                .createCriteria(clazz)
                .list();
    }

    @Override
    @Transactional(readOnly = true)
    public BaseEntity getById(Class clazz, Long id) {
        return (BaseEntity) sessionFactory.getCurrentSession().get(clazz, id);
    }

    @Override
    @Transactional(readOnly = true)
    public SorguSonucu getAll(Class clazz, int first, int pageSize, String sortField,
                              SortOrder sortOrder, Map<String, Object> filters) {

        SorguSonucu ss = new SorguSonucu();

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(clazz);
        if (!filters.keySet().isEmpty()) {
            for (String key : filters.keySet()) {
                criteria.add(Restrictions.ilike(key, (String) filters.get(key), MatchMode.ANYWHERE));
            }
        }
        criteria.setProjection(Projections.rowCount());
        ss.setRowCount(((Long) criteria.uniqueResult()).intValue());

        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setProjection(null);
        if (sortField != null && sortField.equals("")) {
            if (sortOrder.equals(SortOrder.ASCENDING)) {
                criteria.addOrder(Order.asc(sortField));
            } else {
                criteria.addOrder(Order.desc(sortField));
            }
        }
        criteria.setFirstResult(first);
        criteria.setFetchSize(pageSize);
        ss.setList(criteria.list());

        return ss;
    }
}