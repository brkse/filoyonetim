package com.burakkutbay.freamework;

import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */

@SuppressWarnings("rawtypes")
public class SorguSonucu {
    private List list;
    private int rowCount;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }
}
