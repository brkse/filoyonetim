package com.burakkutbay.proje.fatura;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Cell;
import be.quodlibet.boxable.Row;
import com.burakkutbay.freamework.GenericDao;
import com.burakkutbay.proje.arac.Arac;
import com.burakkutbay.proje.kurum.Kurum;
import com.burakkutbay.proje.kurum.KurumDao;
import com.burakkutbay.proje.kurum.KurumService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by burak on 03.04.2017.
 */
@Controller("faturaController")
@Scope("view")
@SuppressWarnings("unchecked")
public class FaturaController {

    @Autowired
    private GenericDao genericDao;

    @Autowired
    private KurumDao kurumDao;

    @Autowired
    private KurumService kurumService;
    private StreamedContent file;
    private StreamedContent faturaPdf;
    private Kurum kurum;
    private Kurum kurumFatura;

    private List<Kurum> tumKurumListesi;
    private List<Kurum> kurumListesi;

    @PostConstruct
    public void init() {
        kurumListesi = new ArrayList<Kurum>();
        this.tumKurumListesi = genericDao.getAll(Kurum.class);
        for (Kurum k : this.tumKurumListesi) {
            if (!k.getAracListesi().isEmpty()) {
                kurumListesi.add(k);
            }
        }
    }

    public void sozlesmeOlustur() {

        String kurumadi = kurum.getAd();
        kurumadi = kurumadi.replace('ö', 'o');
        kurumadi = kurumadi.replace('ü', 'u');
        kurumadi = kurumadi.replace('ğ', 'g');
        kurumadi = kurumadi.replace('ş', 's');
        kurumadi = kurumadi.replace('ı', 'i');
        kurumadi = kurumadi.replace('ç', 'c');
        kurumadi = kurumadi.replace('Ö', 'O');
        kurumadi = kurumadi.replace('Ü', 'U');
        kurumadi = kurumadi.replace('Ğ', 'G');
        kurumadi = kurumadi.replace('Ş', 'S');
        kurumadi = kurumadi.replace('İ', 'I');
        kurumadi = kurumadi.replace('Ç', 'C');


        try (PDDocument doc = new PDDocument()) {

            PDPage page = new PDPage();
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            PDFont pdfFont = PDType1Font.HELVETICA;
            float fontSize = 10;
            float leading = 1.5f * fontSize;

            PDRectangle mediabox = page.getMediaBox();
            float margin = 50;
            float width = mediabox.getWidth() - 2 * margin;
            float startX = mediabox.getLowerLeftX() + margin;
            float startY = mediabox.getUpperRightY() - margin;

            String text1 = "            1. Yukarida kayitlari yazili arac/araclarin 1 yil gecerli olmak uzere kira sozlesmesi akdolunmustur. ";
            String text2 = "            2. Kira bedeli yillik toplam " + kurum.getAracListesi().size() * 2700 * 12 + " TL'dir. Bir aylik kira tutari her ayin sonunda kiraci tarafindan tasit sahibine odenecektir. ";
            String text3 = "            3. Kiralanan tasitin, Akaryakit, Bakim-Onarim, lastik giderleri, park, garaj giderleri, tasitin vergisi, resim, harclari, sofor ve yardimcilarinin maaslari kiraci tarafindan karsilanacaktir.";
            String text4 = "            4. Tasinacak emtianin sigorta giderleri kiraci tarafindan karsilanacaktir.";
            String text5 = "            5. Isbu sozlesme taraflarin karsilikli kabulu ile feshedilebilir. Taraflardan birinin sozlesme hukumlerine aykiri davranmasi halinde diger taraf lehine tek tarafli olarak da fesih hakki dogar. " + LocalDate.now() + "";
            List<String> lines = new ArrayList<String>();
            int lastSpace = -1;
            while (text1.length() > 0) {
                int spaceIndex = text1.indexOf(' ', lastSpace + 1);
                if (spaceIndex < 0)
                    spaceIndex = text1.length();
                String subString = text1.substring(0, spaceIndex);
                float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
                System.out.printf("'%s' - %f of %f\n", subString, size, width);
                if (size > width) {
                    if (lastSpace < 0)
                        lastSpace = spaceIndex;
                    subString = text1.substring(0, lastSpace);
                    lines.add(subString);
                    text1 = text1.substring(lastSpace).trim();
                    System.out.printf("'%s' is line\n", subString);
                    lastSpace = -1;
                } else if (spaceIndex == text1.length()) {
                    lines.add(text1);
                    System.out.printf("'%s' is line\n", text1);
                    text1 = "";
                } else {
                    lastSpace = spaceIndex;
                }
            }
            ////////////////////////////////////////////////////////////////////////////////
            List<String> lines2 = new ArrayList<String>();
            int lastSpace2 = -1;
            while (text2.length() > 0) {
                int spaceIndex2 = text2.indexOf(' ', lastSpace2 + 1);
                if (spaceIndex2 < 0)
                    spaceIndex2 = text2.length();
                String subString = text2.substring(0, spaceIndex2);
                float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
                System.out.printf("'%s' - %f of %f\n", subString, size, width);
                if (size > width) {
                    if (lastSpace2 < 0)
                        lastSpace2 = spaceIndex2;
                    subString = text2.substring(0, lastSpace2);
                    lines2.add(subString);
                    text2 = text2.substring(lastSpace2).trim();
                    System.out.printf("'%s' is line\n", subString);
                    lastSpace2 = -1;
                } else if (spaceIndex2 == text2.length()) {
                    lines2.add(text2);
                    System.out.printf("'%s' is line\n", text2);
                    text2 = "";
                } else {
                    lastSpace2 = spaceIndex2;
                }
            }
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            List<String> lines3 = new ArrayList<String>();
            int lastSpace3 = -1;
            while (text3.length() > 0) {
                int spaceIndex3 = text3.indexOf(' ', lastSpace3 + 1);
                if (spaceIndex3 < 0)
                    spaceIndex3 = text3.length();
                String subString = text3.substring(0, spaceIndex3);
                float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
                System.out.printf("'%s' - %f of %f\n", subString, size, width);
                if (size > width) {
                    if (lastSpace3 < 0)
                        lastSpace3 = spaceIndex3;
                    subString = text3.substring(0, lastSpace3);
                    lines3.add(subString);
                    text3 = text3.substring(lastSpace3).trim();
                    System.out.printf("'%s' is line\n", subString);
                    lastSpace3 = -1;
                } else if (spaceIndex3 == text3.length()) {
                    lines3.add(text3);
                    System.out.printf("'%s' is line\n", text3);
                    text3 = "";
                } else {
                    lastSpace3 = spaceIndex3;
                }
            }
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            List<String> lines4 = new ArrayList<String>();
            int lastSpace4 = -1;
            while (text4.length() > 0) {
                int spaceIndex4 = text4.indexOf(' ', lastSpace4 + 1);
                if (spaceIndex4 < 0)
                    spaceIndex4 = text4.length();
                String subString = text4.substring(0, spaceIndex4);
                float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
                System.out.printf("'%s' - %f of %f\n", subString, size, width);
                if (size > width) {
                    if (lastSpace4 < 0)
                        lastSpace4 = spaceIndex4;
                    subString = text4.substring(0, lastSpace4);
                    lines4.add(subString);
                    text4 = text4.substring(lastSpace4).trim();
                    System.out.printf("'%s' is line\n", subString);
                    lastSpace4 = -1;
                } else if (spaceIndex4 == text4.length()) {
                    lines4.add(text4);
                    System.out.printf("'%s' is line\n", text4);
                    text4 = "";
                } else {
                    lastSpace4 = spaceIndex4;
                }
            }
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            List<String> lines5 = new ArrayList<String>();
            int lastSpace5 = -1;
            while (text5.length() > 0) {
                int spaceIndex5 = text5.indexOf(' ', lastSpace5 + 1);
                if (spaceIndex5 < 0)
                    spaceIndex5 = text5.length();
                String subString = text5.substring(0, spaceIndex5);
                float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
                System.out.printf("'%s' - %f of %f\n", subString, size, width);
                if (size > width) {
                    if (lastSpace5 < 0)
                        lastSpace5 = spaceIndex5;
                    subString = text5.substring(0, lastSpace5);
                    lines5.add(subString);
                    text5 = text5.substring(lastSpace5).trim();
                    System.out.printf("'%s' is line\n", subString);
                    lastSpace5 = -1;
                } else if (spaceIndex5 == text5.length()) {
                    lines5.add(text5);
                    System.out.printf("'%s' is line\n", text5);
                    text5 = "";
                } else {
                    lastSpace5 = spaceIndex5;
                }
            }
            ////////////////////////////////////////////////////////////////////////////////


            contentStream.beginText();
            contentStream.newLineAtOffset(startX, startY);
            contentStream.setFont(pdfFont, 25);
            contentStream.setLeading(14.5f);
            contentStream.newLine();
            contentStream.newLine();
            contentStream.showText("                Arac Kiralama Sozlesmesi");
            contentStream.newLine();
            contentStream.newLine();
            contentStream.newLine();
            contentStream.newLine();
            contentStream.setFont(pdfFont, 11);
            contentStream.showText("KIRALANAN ARAC LISTESI");
            contentStream.newLine();
            int say = 0;
            contentStream.setFont(pdfFont, 10);
            //for (Arac a : kurum.getAracListesi()) {
            for (Arac a : kurum.getAracListesi()) {
                say++;
                String marka = String.valueOf(a.getMarka().getAdi());
                marka = marka.replace('ö', 'o');
                marka = marka.replace('ü', 'u');
                marka = marka.replace('ğ', 'g');
                marka = marka.replace('ş', 's');
                marka = marka.replace('ı', 'i');
                marka = marka.replace('ç', 'c');
                marka = marka.replace('Ö', 'O');
                marka = marka.replace('Ü', 'U');
                marka = marka.replace('Ğ', 'G');
                marka = marka.replace('Ş', 'S');
                marka = marka.replace('İ', 'I');
                marka = marka.replace('Ç', 'C');
                String model = a.getModel();
                model = model.replace('ö', 'o');
                model = model.replace('ü', 'u');
                model = model.replace('ğ', 'g');
                model = model.replace('ş', 's');
                model = model.replace('ı', 'i');
                model = model.replace('ç', 'c');
                model = model.replace('Ö', 'O');
                model = model.replace('Ü', 'U');
                model = model.replace('Ğ', 'G');
                model = model.replace('Ş', 'S');
                model = model.replace('İ', 'I');
                model = model.replace('Ç', 'C');
                String plaka = a.getPlaka();
                plaka = plaka.replace('ö', 'o');
                plaka = plaka.replace('ü', 'u');
                plaka = plaka.replace('ğ', 'g');
                plaka = plaka.replace('ş', 's');
                plaka = plaka.replace('ı', 'i');
                plaka = plaka.replace('ç', 'c');
                plaka = plaka.replace('Ö', 'O');
                plaka = plaka.replace('Ü', 'U');
                plaka = plaka.replace('Ğ', 'G');
                plaka = plaka.replace('Ş', 'S');
                plaka = plaka.replace('İ', 'I');
                plaka = plaka.replace('Ç', 'C');
                contentStream.showText(say + ". " + marka + " - " + model + " - " + plaka);
                contentStream.newLine();
            }
            contentStream.newLine();
            contentStream.newLine();
            contentStream.setFont(pdfFont, fontSize);
            contentStream.showText("    Sayin Yetkili ");
            contentStream.newLine();
            for (String line : lines) {
                contentStream.showText(line); //text1 i yazdırıyor
                contentStream.newLineAtOffset(0, -leading);
            }
            contentStream.newLine();
            for (String line2 : lines2) {
                contentStream.showText(line2); //text2 i yazdırıyor
                contentStream.newLineAtOffset(0, -leading);
            }
            contentStream.newLine();
            for (String line3 : lines3) {
                contentStream.showText(line3); //text2 i yazdırıyor
                contentStream.newLineAtOffset(0, -leading);
            }
            contentStream.newLine();
            for (String line4 : lines4) {
                contentStream.showText(line4); //text2 i yazdırıyor
                contentStream.newLineAtOffset(0, -leading);
            }
            contentStream.newLine();
            for (String line5 : lines5) {
                contentStream.showText(line5); //text2 i yazdırıyor
                contentStream.newLineAtOffset(0, -leading);
            }
            contentStream.newLine();
            contentStream.newLine();
            contentStream.showText("                      Burak Filo Temsilcisi   ");
            contentStream.newLine();
            contentStream.showText("                           Kase / Imza        ");
            contentStream.newLine();
            contentStream.showText("                                                                                                                                " + kurumadi);
            contentStream.newLine();
            contentStream.showText("                                                                                                                                 Kase / Imza ");
            contentStream.endText();
            contentStream.close();

            doc.save("c:/PdfCikti/HelloPDFBox.pdf");
            doc.close();


            File yol = new File("C:/PdfCikti/HelloPDFBox.pdf");
            InputStream stream = new FileInputStream(yol);
            file = new DefaultStreamedContent(stream, "application/pdf", "Sozlesme.pdf");


        } catch (IOException ioEx) {
            System.out.println("Exception while trying to create simple document - " + ioEx);
        }

    }

    public void faturaOlustur() {

        try (PDDocument doc = new PDDocument()) {

            PDPage page = new PDPage();
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            contentStream.beginText();
            contentStream.setLeading(14.5f);
            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 22);
            contentStream.moveTextPositionByAmount(50, 700);
            contentStream.drawString("Burak Filo A.S");
            contentStream.newLine();
            contentStream.setFont(PDType1Font.HELVETICA, 18);
            contentStream.drawString("                                         FATURA");
            //////////////////////////////// Tablo Oluştur  Başla \\\\\\\\\\\\\\\\\\\\\\\\\\

            //Dummy Table
            float margin = 50;
            // starting y position is whole page height subtracted by top and bottom margin
            float yStartNewPage = page.getMediaBox().getHeight() - (2 * margin);
            // we want table across whole page width (subtracted by left and right margin ofcourse)
            float tableWidth = page.getMediaBox().getWidth() - (2 * margin);

            boolean drawContent = true;
            float yStart = yStartNewPage;
            float bottomMargin = 70;
            // y position is your coordinate of top left corner of the table
            float yPosition = 550;

            BaseTable table = new BaseTable(550, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, true, drawContent);
            Row<PDPage> headerRow = table.createRow(15f);
            Cell<PDPage> cell = headerRow.createCell(100, "Arac Bilgileri");
            table.addHeaderRow(headerRow);


            Row<PDPage> row = table.createRow(12);

            cell = row.createCell(30, "Marka");
            cell = row.createCell(30, "Model");
            cell = row.createCell(40, "Plaka");


            ////////////////////////////////////////////////////////

            for (Arac a : kurumFatura.getAracListesi()) {

                Row<PDPage> ad = table.createRow(12);
                String marka = String.valueOf(a.getMarka().getAdi());
                marka = marka.replace('ö', 'o');
                marka = marka.replace('ü', 'u');
                marka = marka.replace('ğ', 'g');
                marka = marka.replace('ş', 's');
                marka = marka.replace('ı', 'i');
                marka = marka.replace('ç', 'c');
                marka = marka.replace('Ö', 'O');
                marka = marka.replace('Ü', 'U');
                marka = marka.replace('Ğ', 'G');
                marka = marka.replace('Ş', 'S');
                marka = marka.replace('İ', 'I');
                marka = marka.replace('Ç', 'C');
                cell = ad.createCell(30, marka);
                String model = a.getModel();
                model = model.replace('ö', 'o');
                model = model.replace('ü', 'u');
                model = model.replace('ğ', 'g');
                model = model.replace('ş', 's');
                model = model.replace('ı', 'i');
                model = model.replace('ç', 'c');
                model = model.replace('Ö', 'O');
                model = model.replace('Ü', 'U');
                model = model.replace('Ğ', 'G');
                model = model.replace('Ş', 'S');
                model = model.replace('İ', 'I');
                model = model.replace('Ç', 'C');
                cell = ad.createCell(30, model);
                String plaka = a.getPlaka();
                plaka = plaka.replace('ö', 'o');
                plaka = plaka.replace('ü', 'u');
                plaka = plaka.replace('ğ', 'g');
                plaka = plaka.replace('ş', 's');
                plaka = plaka.replace('ı', 'i');
                plaka = plaka.replace('ç', 'c');
                plaka = plaka.replace('Ö', 'O');
                plaka = plaka.replace('Ü', 'U');
                plaka = plaka.replace('Ğ', 'G');
                plaka = plaka.replace('Ş', 'S');
                plaka = plaka.replace('İ', 'I');
                plaka = plaka.replace('Ç', 'C');
                cell = ad.createCell(40, plaka);

            }

            Row<PDPage> footer = table.createRow(12);
            cell = footer.createCell(30, "");
            cell = footer.createCell(30, "Tutar : ");
            cell = footer.createCell(40, String.valueOf(kurumFatura.getAracListesi().size() * 2700 * 12) + " Turk Lirasi");

            table.draw();
            contentStream.close();
            contentStream.endText();

            doc.save("c:/PdfCikti/HelloPDFBox.pdf");
            doc.close();

            File yol = new File("C:/PdfCikti/HelloPDFBox.pdf");
            InputStream stream = new FileInputStream(yol);
            faturaPdf = new DefaultStreamedContent(stream, "application/pdf", "Fatura.pdf");


        } catch (IOException ioEx) {
            System.out.println("Exception while trying to create simple document - " + ioEx);
        }

    }

    public List<Kurum> getKurumListesi() {
        return kurumListesi;
    }

    public void setKurumListesi(List<Kurum> kurumListesi) {
        this.kurumListesi = kurumListesi;
    }

    public List<Kurum> getTumKurumListesi() {
        return tumKurumListesi;
    }

    public void setTumKurumListesi(List<Kurum> tumKurumListesi) {
        this.tumKurumListesi = tumKurumListesi;
    }

    public StreamedContent getFile() throws IOException {
        sozlesmeOlustur();
        return file;
    }

    public StreamedContent getFaturaPdf() throws IOException {
        faturaOlustur();
        return faturaPdf;
    }

    public void setFaturaPdf(StreamedContent faturaPdf) {
        this.faturaPdf = faturaPdf;
    }

    public Kurum getKurum() {
        return kurum;
    }

    public void setKurum(Kurum kurum) {
        this.kurum = kurum;
    }

    public Kurum getKurumFatura() {
        return kurumFatura;
    }

    public void setKurumFatura(Kurum kurumFatura) {
        this.kurumFatura = kurumFatura;
    }
}
