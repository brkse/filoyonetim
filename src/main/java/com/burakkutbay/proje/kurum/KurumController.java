package com.burakkutbay.proje.kurum;

import com.burakkutbay.freamework.GenericDao;
import com.burakkutbay.proje.arac.Arac;
import com.burakkutbay.proje.common.GlobalDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Controller("kurumController")
@Scope("view")
@SuppressWarnings("unchecked")
public class KurumController {

    @Autowired
    private GlobalDataService globalDataService;

    @Autowired
    private GenericDao genericDao;

    @Autowired
    private KurumDao kurumDao;

    @Autowired
    private KurumService kurumService;

    private List<Kurum> kurumListesi;

    private List<Arac> eklenebilirAracListesi;

    private boolean yeniKayit = false;

    private Kurum kurum;

    private List<Arac> secilenArabaListesi;

    @PostConstruct
    public void init() {

        refreshKurumListesi();
    }

    private void refreshKurumListesi() {

        this.kurumListesi = genericDao.getAll(Kurum.class);
    }

    //CRUD İşlemleri Başla

    public void vazgec() {

        kurum = null;
        yeniKayit = false;

    }

    public void sil() {

        genericDao.delete(kurum);
        kurum = null;
        yeniKayit = false;
        refreshKurumListesi();

    }

    public void kaydet() {

        if (yeniKayit) {
            kurumService.save(kurum);
        } else {
            genericDao.update(kurum);
        }

        yeniKayit = false;
        kurum = null;
        refreshKurumListesi();

    }

    public void topluKaydet() {

        List<Arac> aracListesi = new ArrayList<Arac>();

        for (int i = 0; i < 10; i++) {
            Kurum k = new Kurum();
            k.setAd("Kurum : " + i);
            kurumListesi.add(k);
        }
        try {
            kurumService.topluKaydet(kurumListesi);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void yeni() {
        this.kurum = new Kurum();
        this.yeniKayit = true;
        this.eklenebilirAracListesi = kurumDao.getEklenebilirAracListesi(this.kurum);
    }

    public void secilenleriEkle() {
        for (Arac arac : secilenArabaListesi) {
            this.kurum.addArac(arac);
        }
    }

    public void arabaSil(Arac arac) {
        this.kurum.removeArac(arac);
        this.eklenebilirAracListesi.add(arac);
    }

    //CRUD İşlemleri Bit


    //Getter && Setter

    public List<Kurum> getKurumListesi() {
        return kurumListesi;
    }

    public void setKurumListesi(List<Kurum> kurumListesi) {
        this.kurumListesi = kurumListesi;
    }

    public boolean isYeniKayit() {
        return yeniKayit;
    }

    public void setYeniKayit(boolean yeniKayit) {
        this.yeniKayit = yeniKayit;
    }

    public Kurum getKurum() {
        return kurum;
    }

    public void setKurum(Kurum kurum) {
        this.kurum = (Kurum) genericDao.getById(Kurum.class, kurum.getId());
        this.eklenebilirAracListesi = kurumDao.getEklenebilirAracListesi(this.kurum);
        this.yeniKayit = false;
    }

    public List<Arac> getEklenebilirAracListesi() {
        return eklenebilirAracListesi;
    }

    public void setEklenebilirAracListesi(List<Arac> eklenebilirAracListesi) {
        this.eklenebilirAracListesi = eklenebilirAracListesi;
    }

    public List<Arac> getSecilenArabaListesi() {
        return secilenArabaListesi;
    }

    public void setSecilenArabaListesi(List<Arac> secilenArabaListesi) {
        this.secilenArabaListesi = secilenArabaListesi;
    }
}
