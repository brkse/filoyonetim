package com.burakkutbay.proje.kurum;

import com.burakkutbay.proje.arac.Arac;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Repository("kurumDao")
@Transactional(readOnly = true)
@SuppressWarnings("unchecked")
public class KurumDaoImpl implements KurumDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Arac> getEklenebilirAracListesi(Kurum kurum) {
        List<Long> idListesi = new ArrayList<Long>();
        for (Arac arac : kurum.getAracListesi()) {
            idListesi.add(arac.getId());
        }
        if (idListesi.isEmpty()) {
            return sessionFactory.getCurrentSession()
                    .createCriteria(Arac.class)
                    .list();
        } else {
            return sessionFactory.getCurrentSession()
                    .createCriteria(Arac.class)
                    .add(Restrictions.not(Restrictions.in("id", idListesi)))
                    .list();
        }
    }

    @Override
    public Long kurumSayisi() {
        return (Long) sessionFactory.getCurrentSession().createCriteria(Kurum.class).setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public String enCokArabaBulunanKurum() {

        String q1 = "select t_kurum.ad from t_kurum_arac inner join t_kurum on t_kurum_arac.kurum_id=t_kurum.id inner join t_arac on t_arac.id=t_kurum_arac.arac_id group by t_kurum.ad order by count(*) desc";
        List<Object[]> rows = sessionFactory.getCurrentSession().createSQLQuery(q1).list();
        //ArrayList<Kurum> list = (ArrayList<Kurum>) query.list();
        //List<Object[]> rows = query.list();
    /*    Kurum kurum= new Kurum();
        for(Object[] row : rows){
            kurum.setAd(row[0].toString());;
            System.out.println(kurum);
        }*/
        //System.out.println(list.get(0).getAd());
        String kurum = String.valueOf(rows.get(0));

        return kurum;
    }
}
