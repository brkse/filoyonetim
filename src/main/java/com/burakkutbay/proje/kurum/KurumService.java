package com.burakkutbay.proje.kurum;

import com.burakkutbay.freamework.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Service("kurumService")
public class KurumService {

    @Autowired
    private GenericDao genericDao;

    public void save(Kurum kurum) {
        genericDao.save(kurum);
    }

    public void topluKaydet(List<Kurum> kurumListesi) {
        for (Kurum k : kurumListesi) {
            genericDao.save(k);
        }
    }
}
