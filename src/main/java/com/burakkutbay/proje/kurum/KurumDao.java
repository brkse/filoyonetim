package com.burakkutbay.proje.kurum;

import com.burakkutbay.proje.arac.Arac;

import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
public interface KurumDao {
    List<Arac> getEklenebilirAracListesi(Kurum kurum);

    Long kurumSayisi();

    String enCokArabaBulunanKurum();
}
