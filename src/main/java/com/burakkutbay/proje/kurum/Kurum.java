package com.burakkutbay.proje.kurum;

import com.burakkutbay.freamework.BaseEntity;
import com.burakkutbay.proje.arac.Arac;
import com.burakkutbay.proje.il.Il;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Entity
@Table(name = "t_kurum")
public class Kurum extends BaseEntity {

    private String ad;
    private List<Arac> aracListesi = new ArrayList<Arac>();
    private Date kiralanmaTarihi;
    private Il kurumIli;

    @Column(nullable = false)
    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "t_kurum_arac",
            joinColumns = @JoinColumn(name = "kurum_id"),
            inverseJoinColumns = @JoinColumn(name = "arac_id"))
    public List<Arac> getAracListesi() {
        return aracListesi;
    }

    public void setAracListesi(List<Arac> aracListesi) {
        this.aracListesi = aracListesi;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getKiralanmaTarihi() {
        return kiralanmaTarihi;
    }

    public void setKiralanmaTarihi(Date kiralanmaTarihi) {
        this.kiralanmaTarihi = kiralanmaTarihi;
    }

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_kurum_il"))
    public Il getKurumIli() {
        return kurumIli;
    }

    public void setKurumIli(Il kurumIli) {
        this.kurumIli = kurumIli;
    }

    public void addArac(Arac arac) {
        if (!aracListesi.contains(arac)) {
            aracListesi.add(arac);
        }
    }

    public void removeArac(Arac arac) {
        aracListesi.remove(arac);
    }

}
