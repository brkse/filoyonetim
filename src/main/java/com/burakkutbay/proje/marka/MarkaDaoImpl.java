package com.burakkutbay.proje.marka;

import com.burakkutbay.freamework.SorguSonucu;
import com.burakkutbay.proje.arac.Arac;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * Created by hasanburakkutbay on 27.03.2017.
 */
@Repository("markaDao")
@Scope("singleton")
@Transactional(readOnly = true)
public class MarkaDaoImpl implements MarkaDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Marka> getByKeyword(String keyword) {
        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(Marka.class)
                .add(Restrictions.or(Restrictions.ilike("adi", keyword, MatchMode.ANYWHERE)))
                .addOrder(Order.asc("adi"));
        return criteria.list();
    }

    @Override
    public Marka getMarka(Long id) {
        Marka marka = (Marka) sessionFactory.getCurrentSession().get(Marka.class, id);
        for (Arac a : marka.getAracListesi()) {
            System.out.println(a);
        }
        return marka;
    }

    @Override
    public Marka getByAd(String ad) {
        //NamedQuery Eklenebilir.
        return null;
    }

    @Override
    public SorguSonucu getMarkaListesi(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        SorguSonucu ss = new SorguSonucu();

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Marka.class);
        criteria.setFetchMode("adi", FetchMode.JOIN);
        if (!filters.keySet().isEmpty()) {
            for (String key : filters.keySet()) {
                criteria.add(Restrictions.ilike(key, (String) filters.get(key), MatchMode.ANYWHERE));
            }
        }
        criteria.setProjection(Projections.rowCount());
        ss.setRowCount(((Long) criteria.uniqueResult()).intValue());

        criteria.setProjection(null);
        if (sortField != null && sortField.equals("")) {
            if (sortOrder.equals(SortOrder.ASCENDING)) {
                criteria.addOrder(Order.asc(sortField));
            } else {
                criteria.addOrder(Order.desc(sortField));
            }
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setFirstResult(first);
        criteria.setFetchSize(pageSize);
        ss.setList(criteria.list());

        return ss;
    }

    @Override
    public String enCokBulunanArabaMarka() {
        String q1 = "select t_marka.adi from t_arac inner join t_marka on t_arac.marka_id=t_marka.id group by t_marka.adi order by count(*) desc";
        List<Object[]> rows = sessionFactory.getCurrentSession().createSQLQuery(q1).list();
        String arac = String.valueOf(rows.get(0));
        return arac;
    }
}
