package com.burakkutbay.proje.marka;

import com.burakkutbay.freamework.BaseEntity;
import com.burakkutbay.freamework.BaseEntityLazyDataModel;
import com.burakkutbay.freamework.GenericDao;
import com.burakkutbay.freamework.SorguSonucu;
import com.burakkutbay.proje.common.GlobalDataService;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 27.03.2017.
 */
@Controller("markaController")
@Scope("view")
@SuppressWarnings({"serial"})
public class MarkaController implements Serializable {

    @Autowired
    private GlobalDataService globalDataService;

    @Autowired
    private transient GenericDao genericDao;

    @Autowired
    private transient MarkaDao markaDao;

    private Marka marka;

    public Marka getMarka() {
        return marka;
    }

    public void setMarka(Marka marka) {
        this.marka = (Marka) genericDao.getById(Marka.class, marka.getId());
        this.yeniKayit = false;
    }

    private BaseEntityLazyDataModel markaLazyDataModel;

    public BaseEntityLazyDataModel getMarkaLazyDataModel() {
        return markaLazyDataModel;
    }

    @PostConstruct
    public void init() {
        markaLazyDataModel = new MarkaLazyDataModel();
    }

    private boolean yeniKayit = false;

    public boolean isYeniKayit() {
        return yeniKayit;
    }

    public void vazgec() {
        marka = null;
        yeniKayit = false;
    }

    public void sil() {
        genericDao.delete(marka);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Silindi.", marka.getAdi());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        marka = null;
        yeniKayit = false;
        globalDataService.refreshMarkaListesi();
    }

    public void kaydet() {
        try {
            if (yeniKayit) {
                genericDao.save(marka);
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Kaydedildi.", marka.getAdi());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            } else {
                genericDao.update(marka);
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Güncellendi.", marka.getAdi());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kaydedilemedi.", marka.getAdi());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        yeniKayit = false;
        marka = null;
        globalDataService.refreshMarkaListesi();
    }

    public void yeni() {
        this.marka = new Marka();
        this.yeniKayit = true;
    }

    public void onRowSelect(SelectEvent event) {
        this.setMarka((Marka) event.getObject());
    }

    private class MarkaLazyDataModel extends BaseEntityLazyDataModel {

        @SuppressWarnings("unchecked")
        @Override
        public List<BaseEntity> load(int first, int pageSize, String sortField,
                                     SortOrder sortOrder, Map<String, Object> filters) {
            SorguSonucu ss = markaDao.getMarkaListesi(first, pageSize, sortField, sortOrder, filters);
            this.setRowCount(ss.getRowCount());
            return ss.getList();
        }

    }
}
