package com.burakkutbay.proje.marka;

import com.burakkutbay.freamework.SorguSonucu;
import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 27.03.2017.
 */
public interface MarkaDao {
    List<Marka> getByKeyword(String keyword);

    Marka getMarka(Long id);

    Marka getByAd(String ad);

    SorguSonucu getMarkaListesi(int first, int pageSize, String sortField,
                                SortOrder sortOrder, Map<String, Object> filters);

    String enCokBulunanArabaMarka();
}
