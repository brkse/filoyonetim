package com.burakkutbay.proje.marka;

import com.burakkutbay.freamework.BaseEntity;
import com.burakkutbay.proje.arac.Arac;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by hasanburakkutbay on 27.03.2017.
 */
@Entity
@Table(name = "t_marka")
public class Marka extends BaseEntity {

    private String adi;
    private List<Arac> aracListesi;

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    @OneToMany(mappedBy = "marka", orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Arac> getAracListesi() {
        return aracListesi;
    }

    public void setAracListesi(List<Arac> aracListesi) {
        this.aracListesi = aracListesi;
    }
}
