package com.burakkutbay.proje.marka;

import com.burakkutbay.freamework.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by hasanburakkutbay on 27.03.2017.
 */
@Controller("markaConverter")
@Scope("request")
public class MarkaConverter implements Converter {

    @Autowired
    private GenericDao genericDao;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String str) {
        if (str != null && !str.equals("")) {
            Long id = Long.parseLong(str);
            return genericDao.getById(Marka.class, id);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object obj) {
        if (obj != null) {
            Marka marka = (Marka) obj;
            return String.valueOf(marka.getId());
        }
        return "";
    }
}
