package com.burakkutbay.proje.islem;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hasanburakkutbay on 5.04.2017.
 */

@Controller("teklifController")
@Scope("view")
@SuppressWarnings({"serial"})
public class TeklifBeanController {

    @Autowired
    private Teklif teklifBean;
    private StreamedContent file;


    public void teklifHazirla() throws IOException {


        String kurumadi = teklifBean.getKurumAdi();
        kurumadi = kurumadi.replace('ö', 'o');
        kurumadi = kurumadi.replace('ü', 'u');
        kurumadi = kurumadi.replace('ğ', 'g');
        kurumadi = kurumadi.replace('ş', 's');
        kurumadi = kurumadi.replace('ı', 'i');
        kurumadi = kurumadi.replace('ç', 'c');
        kurumadi = kurumadi.replace('Ö', 'O');
        kurumadi = kurumadi.replace('Ü', 'U');
        kurumadi = kurumadi.replace('Ğ', 'G');
        kurumadi = kurumadi.replace('Ş', 'S');
        kurumadi = kurumadi.replace('İ', 'I');
        kurumadi = kurumadi.replace('Ç', 'C');

        String musteriTemsilcisi = teklifBean.getMusteriTemsilcisi();
        musteriTemsilcisi = musteriTemsilcisi.replace('ö', 'o');
        musteriTemsilcisi = musteriTemsilcisi.replace('ü', 'u');
        musteriTemsilcisi = musteriTemsilcisi.replace('ğ', 'g');
        musteriTemsilcisi = musteriTemsilcisi.replace('ş', 's');
        musteriTemsilcisi = musteriTemsilcisi.replace('ı', 'i');
        musteriTemsilcisi = musteriTemsilcisi.replace('ç', 'c');
        musteriTemsilcisi = musteriTemsilcisi.replace('Ö', 'O');
        musteriTemsilcisi = musteriTemsilcisi.replace('Ü', 'U');
        musteriTemsilcisi = musteriTemsilcisi.replace('Ğ', 'G');
        musteriTemsilcisi = musteriTemsilcisi.replace('Ş', 'S');
        musteriTemsilcisi = musteriTemsilcisi.replace('İ', 'I');
        musteriTemsilcisi = musteriTemsilcisi.replace('Ç', 'C');


        try (PDDocument doc = new PDDocument()) {

            PDPage page = new PDPage();
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            PDFont pdfFont = PDType1Font.HELVETICA;
            float fontSize = 12;
            float leading = 1.5f * fontSize;

            PDRectangle mediabox = page.getMediaBox();
            float margin = 72;
            float width = mediabox.getWidth() - 2*margin;
            float startX = mediabox.getLowerLeftX() + margin;
            float startY = mediabox.getUpperRightY() - margin;

            String text = "    "+ kurumadi +" Kurumunda kullanilmak uzere " + teklifBean.getAracSayisi()+" adet arac icin yillik teklifimiz " +teklifBean.getFiyatTeklifi()+" Turk Lirasidir. ";
            List<String> lines = new ArrayList<String>();
            int lastSpace = -1;
            while (text.length() > 0)
            {
                int spaceIndex = text.indexOf(' ', lastSpace + 1);
                if (spaceIndex < 0)
                    spaceIndex = text.length();
                String subString = text.substring(0, spaceIndex);
                float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
                System.out.printf("'%s' - %f of %f\n", subString, size, width);
                if (size > width)
                {
                    if (lastSpace < 0)
                        lastSpace = spaceIndex;
                    subString = text.substring(0, lastSpace);
                    lines.add(subString);
                    text = text.substring(lastSpace).trim();
                    System.out.printf("'%s' is line\n", subString);
                    lastSpace = -1;
                }
                else if (spaceIndex == text.length())
                {
                    lines.add(text);
                    System.out.printf("'%s' is line\n", text);
                    text = "";
                }
                else
                {
                    lastSpace = spaceIndex;
                }
            }

            contentStream.beginText();
            contentStream.newLineAtOffset(startX, startY);
            contentStream.setFont(pdfFont, 25);
            contentStream.setLeading(14.5f);
            contentStream.newLine();
            contentStream.newLine();
            contentStream.newLine();
            contentStream.showText("Burak Filo A.S");
            contentStream.newLine();
            contentStream.newLine();
            contentStream.newLine();
            contentStream.newLine();
            contentStream.newLine();
            contentStream.setFont(pdfFont, fontSize);
            contentStream.showText("    Sayin Yetkili ");
            contentStream.newLine();
            contentStream.newLine();
            contentStream.newLine();
            for (String line: lines)
            {
                contentStream.showText(line);
                contentStream.newLineAtOffset(0, -leading);
            }
            contentStream.newLine();
            contentStream.showText("    Tesekkur ederiz");
            contentStream.newLine();
            contentStream.showText("                                                                                         Burak Filo Temsilcisi");
            contentStream.newLine();
            contentStream.showText("                                                                                             "+musteriTemsilcisi);
            contentStream.endText();
            contentStream.close();

            doc.save("c:/PdfCikti/HelloPDFBox.pdf");
            doc.close();


            File yol = new File("C:/PdfCikti/HelloPDFBox.pdf");
            InputStream stream = new FileInputStream(yol);
            file = new DefaultStreamedContent(stream, "application/pdf", "Teklif.pdf");


        } catch (IOException ioEx) {
            System.out.println("Exception while trying to create simple document - " + ioEx);
        }

    }


    public Teklif getTeklifBean() {
        return teklifBean;
    }

    public void setTeklifBean(Teklif teklifBean) {
        this.teklifBean = teklifBean;
    }

    public StreamedContent getFile() throws IOException {
        teklifHazirla();
        return file;
    }
}


