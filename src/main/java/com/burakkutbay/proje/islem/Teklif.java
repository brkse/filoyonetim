package com.burakkutbay.proje.islem;

import org.springframework.stereotype.Component;

/**
 * Created by hasanburakkutbay on 5.04.2017.
 */
@Component
public class Teklif {

    private String kurumAdi;
    private int aracSayisi;
    private String fiyatTeklifi;
    private String musteriTemsilcisi;

    public Teklif() {
    }

    public Teklif(String kurumAdi, int aracSayisi, String fiyatTeklifi, String musteriTemsilcisi) {
        this.kurumAdi = kurumAdi;
        this.aracSayisi = aracSayisi;
        this.fiyatTeklifi = fiyatTeklifi;
        this.musteriTemsilcisi = musteriTemsilcisi;
    }

    public String getKurumAdi() {
        return kurumAdi;
    }

    public void setKurumAdi(String kurumAdi) {
        this.kurumAdi = kurumAdi;
    }

    public int getAracSayisi() {
        return aracSayisi;
    }

    public void setAracSayisi(int aracSayisi) {
        this.aracSayisi = aracSayisi;
    }

    public String getFiyatTeklifi() {
        if (aracSayisi != 0) {
            this.fiyatTeklifi = String.valueOf((aracSayisi * 2700) * 12);
        } else {
            this.fiyatTeklifi = "0";
        }
        return fiyatTeklifi;
    }

    public void setFiyatTeklifi(String fiyatTeklifi) {
        this.fiyatTeklifi = fiyatTeklifi;

    }

    public String getMusteriTemsilcisi() {
        return musteriTemsilcisi;
    }

    public void setMusteriTemsilcisi(String musteriTemsilcisi) {
        this.musteriTemsilcisi = musteriTemsilcisi;
    }
}

