package com.burakkutbay.proje.kullanici;

import com.burakkutbay.freamework.BaseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Entity
@Table(name = "t_kullanici")
@NamedQuery(
        name = Kullanici.NQ.getByKullaniciAdi,
        query = "select k from Kullanici k where k.kullaniciAdi=:kullaniciAdi")
public class Kullanici extends BaseEntity implements UserDetails {

    public interface NQ {
        String getByKullaniciAdi = "Kullanici.getByKullaniciAdi";
    }

    private String kullaniciAdi;
    private String parola;
    private String ad;
    private String soyad;
    private boolean aktif = true;

    private List<String> rolListesi = new ArrayList<String>();

    public String getKullaniciAdi() {
        return kullaniciAdi;
    }

    public void setKullaniciAdi(String kullaniciAdi) {
        this.kullaniciAdi = kullaniciAdi;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public boolean isAktif() {
        return aktif;
    }

    public void setAktif(boolean aktif) {
        this.aktif = aktif;
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "t_kullanici_rol")
    @Column(name = "rol")
    public List<String> getRolListesi() {
        return rolListesi;
    }

    public void setRolListesi(List<String> rolListesi) {
        this.rolListesi = rolListesi;
    }

    public void addRol(String rol) {
        if (!this.rolListesi.contains(rol)) {
            this.rolListesi.add(rol);
        }
    }

    public void removeRol(String rol) {
        this.rolListesi.remove(rol);
    }

    @Transient
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
        for (String rol : this.rolListesi) {
            authorities.add(new SimpleGrantedAuthority(rol));
        }
        return authorities;
    }

    @Transient
    @Override
    public String getPassword() {
        return parola;
    }

    @Transient
    @Override
    public String getUsername() {
        return kullaniciAdi;
    }

    @Transient
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Transient
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Transient
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Transient
    @Override
    public boolean isEnabled() {
        return aktif;
    }
}
