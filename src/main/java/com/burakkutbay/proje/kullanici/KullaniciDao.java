package com.burakkutbay.proje.kullanici;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
public interface KullaniciDao {

    Kullanici getByKullaniciAdi(String username);

}
