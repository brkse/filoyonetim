package com.burakkutbay.proje.kullanici;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Repository("kullaniciDao")
@Transactional
public class KullaniciDaoImpl implements KullaniciDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Kullanici getByKullaniciAdi(String username) {
        return (Kullanici) sessionFactory.getCurrentSession()
                .getNamedQuery(Kullanici.NQ.getByKullaniciAdi)
                .setParameter("kullaniciAdi", username)
                .uniqueResult();
    }
}
