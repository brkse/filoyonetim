package com.burakkutbay.proje.il;

import com.burakkutbay.freamework.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Controller("ilConverter")
@Scope("request")
public class IlConverter implements Converter {

    @Autowired
    private GenericDao genericDao;

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String str) {

        if (str != null && !str.equals("")) {
            Long id = Long.parseLong(str);
            return genericDao.getById(Il.class, id);
        }
        return null;

    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object obj) {

        if (obj != null) {
            Il il = (Il) obj;
            return String.valueOf(il.getId());
        }
        return "";

    }
}
