package com.burakkutbay.proje.il;

import com.burakkutbay.freamework.SorguSonucu;
import com.burakkutbay.proje.kurum.Kurum;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Repository
@Transactional(readOnly = true)
public class IlDaoImpl implements IlDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public Il getIl(Long id) {

        Il il = (Il) sessionFactory.getCurrentSession().get(Il.class, id);
        for (Kurum k : il.getBirimListesi()) {
            System.out.println(k);
        }
        return il;
    }

    @Override
    public SorguSonucu getIlListesi(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        SorguSonucu ss = new SorguSonucu();

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Il.class);
        if (!filters.keySet().isEmpty()) {
            for (String key : filters.keySet()) {
                criteria.add(Restrictions.ilike(key, (String) filters.get(key), MatchMode.ANYWHERE));
            }
        }
        criteria.setProjection(Projections.rowCount());
        ss.setRowCount(((Long) criteria.uniqueResult()).intValue());

        criteria.setProjection(null);
        if (sortField != null && sortField.equals("")) {
            if (sortOrder.equals(SortOrder.ASCENDING)) {
                criteria.addOrder(Order.asc(sortField));
            } else {
                criteria.addOrder(Order.desc(sortField));
            }
        }
        criteria.setFirstResult(first);
        criteria.setFetchSize(pageSize);
//		criteria.setFetchMode("kisiListesi", FetchMode.JOIN);
        criteria.createAlias("birimListesi", "birimListesi", JoinType.INNER_JOIN);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        ss.setList(criteria.list());

        for (Il il : (List<Il>) ss.getList()) {
            for (Kurum k : il.getBirimListesi()) {
                System.out.println(k.getAd());
            }
            System.out.println();
        }
        return ss;
    }

    @Override
    public List<Object[]> arabaBulunanIller() {
        String q1 = "select t_il.ad from t_il inner join t_kurum on t_il.id=t_kurum.kurumili_id group by t_il.ad order by count(*) desc";
        List<Object[]> ilListesi = sessionFactory.getCurrentSession().createSQLQuery(q1).list();
        return ilListesi;
    }
}
