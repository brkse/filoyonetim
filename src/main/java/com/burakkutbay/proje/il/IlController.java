package com.burakkutbay.proje.il;

import com.burakkutbay.freamework.BaseEntity;
import com.burakkutbay.freamework.BaseEntityLazyDataModel;
import com.burakkutbay.freamework.GenericDao;
import com.burakkutbay.freamework.SorguSonucu;
import com.burakkutbay.proje.common.GlobalDataService;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 24.03.2017.
 */
@Controller("ilController")
@Scope("view")
@SuppressWarnings({"serial", "unchecked"})
public class IlController {

    @Autowired
    private GlobalDataService globalDataService;

    @Autowired
    private GenericDao genericDao;

    @Autowired
    private IlDao ilDao;

    private Il il;

    public Il getIl() {
        return il;
    }

    public void setIl(Il il) {
        this.il = ilDao.getIl(il.getId());
        this.yeniKayit = false;
    }

    private boolean yeniKayit = false;

    public boolean isYeniKayit() {
        return yeniKayit;
    }

    @PostConstruct
    public void init() {
        this.lazyDataModel = new IlLazyDataModel();
    }

    public void vazgec() {
        il = null;
        yeniKayit = false;
    }

    public void sil() {
        genericDao.delete(il);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Silindi.", il.getAd());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        il = null;
        yeniKayit = false;
        globalDataService.refreshIlListesi();
    }

    public void kaydet() {
        if (yeniKayit) {
            genericDao.save(il);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Kaydedildi.", il.getAd());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            genericDao.update(il);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Güncellendi.", il.getAd());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        yeniKayit = false;
        il = null;
        globalDataService.refreshIlListesi();
    }

    public void yeni() {
        this.il = new Il();
        this.yeniKayit = true;
    }

    private class IlLazyDataModel extends BaseEntityLazyDataModel {

        @Override
        public List<BaseEntity> load(int first, int pageSize, String sortField,
                                     SortOrder sortOrder, Map<String, Object> filters) {
            SorguSonucu ss = ilDao.getIlListesi(first, pageSize, sortField, sortOrder, filters);
            this.setRowCount(ss.getRowCount());
            return ss.getList();
        }

    }

    private BaseEntityLazyDataModel lazyDataModel;

    public BaseEntityLazyDataModel getLazyDataModel() {
        return lazyDataModel;
    }


}
