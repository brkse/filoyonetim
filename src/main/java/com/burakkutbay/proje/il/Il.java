package com.burakkutbay.proje.il;

import com.burakkutbay.freamework.BaseEntity;
import com.burakkutbay.proje.kurum.Kurum;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Entity
@Table(name = "t_il")
public class Il extends BaseEntity {

    private String ad;
    private List<Kurum> birimListesi;

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    @OneToMany(mappedBy = "kurumIli",
            fetch = FetchType.LAZY,
            orphanRemoval = true)
    public List<Kurum> getBirimListesi() {
        return birimListesi;
    }

    public void setBirimListesi(List<Kurum> birimListesi) {
        this.birimListesi = birimListesi;
    }
}
