package com.burakkutbay.proje.il;

import com.burakkutbay.freamework.SorguSonucu;
import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
public interface IlDao {
    Il getIl(Long id);

    SorguSonucu getIlListesi(int first, int pageSize, String sortField,
                             SortOrder sortOrder, Map<String, Object> filters);

    //List<Object[]> arabaBulunanIller();
    List<Object[]> arabaBulunanIller();
}
