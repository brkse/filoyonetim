package com.burakkutbay.proje.model;

import com.burakkutbay.freamework.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by hasanburakkutbay on 27.03.2017.
 */
@Entity
@Table(name = "t_model")
public class Model extends BaseEntity {

}
