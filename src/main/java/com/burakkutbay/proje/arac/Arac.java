package com.burakkutbay.proje.arac;

import com.burakkutbay.freamework.BaseEntity;
import com.burakkutbay.proje.kurum.Kurum;
import com.burakkutbay.proje.marka.Marka;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Entity
@Table(name = "t_arac")
@NamedQueries(value = {
        @NamedQuery(name = "Arac.getByPlaka",
                query = "select arac from Arac arac where arac.plaka= :plaka")
})
public class Arac extends BaseEntity {

    public interface NQ {
        String getByPlaka = "Arac.getByPlaka";
    }

    private String plaka;
    private Marka marka;
    private String model;
    private Date yil;
    private byte[] foto;
    private List<Kurum> kurumListesi;

    public String getPlaka() {
        return plaka;
    }

    public void setPlaka(String plaka) {
        this.plaka = plaka;
    }

    @ManyToOne(optional = false)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_arac_marka"))
    public Marka getMarka() {
        return marka;
    }

    public void setMarka(Marka marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getYil() {
        return yil;
    }

    public void setYil(Date yil) {
        this.yil = yil;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    @ManyToMany(mappedBy = "aracListesi")
    public List<Kurum> getKurumListesi() {
        return kurumListesi;
    }

    public void setKurumListesi(List<Kurum> kurumListesi) {
        this.kurumListesi = kurumListesi;
    }
}
