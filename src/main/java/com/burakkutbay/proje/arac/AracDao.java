package com.burakkutbay.proje.arac;

import com.burakkutbay.freamework.SorguSonucu;
import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
public interface AracDao {

    List<Arac> getByKeyword(String keyword);

    Arac getByPlaka(String plaka);

    SorguSonucu getAracListesi(int first, int pageSize, String sortField,
                               SortOrder sortOrder, Map<String, Object> filters);

    Long filoAracSayisi();

}
