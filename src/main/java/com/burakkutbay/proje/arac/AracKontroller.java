package com.burakkutbay.proje.arac;

import com.burakkutbay.freamework.BaseEntity;
import com.burakkutbay.freamework.BaseEntityLazyDataModel;
import com.burakkutbay.freamework.GenericDao;
import com.burakkutbay.freamework.SorguSonucu;
import com.burakkutbay.proje.common.GlobalDataService;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 24.03.2017.
 */
@Controller("aracController")
@Scope("view")
@SuppressWarnings({"serial"})
public class AracKontroller implements Serializable {

    @Autowired
    private transient GenericDao genericDao;

    @Autowired
    private transient AracDao aracDao;

    @Autowired
    private GlobalDataService globalDataService;

    @Autowired
    private ImageController imageController;

    private Arac arac;

    private boolean yeniKayit = false;

    private BaseEntityLazyDataModel aracLazyDataModel;

    public BaseEntityLazyDataModel getAracLazyDataModel() {
        return aracLazyDataModel;
    }

    @PostConstruct
    public void init() {
        aracLazyDataModel = new AracLazyDataModel();
        globalDataService.setAktifstye("active");
    }

    public void vazgec() {
        arac = null;
        yeniKayit = false;
    }

    public void sil() {
        genericDao.delete(arac);
        arac = null;
        yeniKayit = false;
    }

    public void kaydet() {

        try {
            if (yeniKayit) {
                genericDao.save(arac);
            } else {
                genericDao.update(arac);
            }
        } catch (Exception e) {
        }

        yeniKayit = false;
        arac = null;
    }

    public void yeni() {
        this.arac = new Arac();
        this.yeniKayit = true;
    }


    public Arac getArac() {
        return arac;
    }

    public void setArac(Arac arac) {
        this.arac = (Arac) genericDao.getById(Arac.class, arac.getId());
        imageController.setImage(this.arac.getFoto());
        this.yeniKayit = false;
    }

    public boolean isYeniKayit() {
        return yeniKayit;
    }

    public void setYeniKayit(boolean yeniKayit) {
        this.yeniKayit = yeniKayit;
    }

    private class AracLazyDataModel extends BaseEntityLazyDataModel {

        @SuppressWarnings("unchecked")
        @Override
        public List<BaseEntity> load(int first, int pageSize, String sortField,
                                     SortOrder sortOrder, Map<String, Object> filters) {
            SorguSonucu ss = aracDao.getAracListesi(first, pageSize, sortField, sortOrder, filters);
            this.setRowCount(ss.getRowCount());
            return ss.getList();
        }

    }

    public void onRowSelect(SelectEvent event) {
        this.setArac((Arac) event.getObject());
    }

    public void handleFileUpload(FileUploadEvent event) {
        this.arac.setFoto(event.getFile().getContents());
        imageController.setImage(this.arac.getFoto());
    }

    public StreamedContent getFoto() {
        return new DefaultStreamedContent(new ByteArrayInputStream(this.arac.getFoto()));
    }
}
