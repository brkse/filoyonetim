package com.burakkutbay.proje.arac;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Controller
@Scope("session")
public class ImageController {

    private byte[] image;

    public void setImage(byte[] image) {
        this.image = image;
    }

    public StreamedContent getImage() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();

            if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
                return new DefaultStreamedContent();
            } else if (image != null) {
                return new DefaultStreamedContent(new ByteArrayInputStream(this.image));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
