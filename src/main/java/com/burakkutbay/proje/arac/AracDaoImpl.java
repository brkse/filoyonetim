package com.burakkutbay.proje.arac;

import com.burakkutbay.freamework.SorguSonucu;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
@Repository("aracDao")
@Scope("singleton")
@Transactional(readOnly = true)
@SuppressWarnings("unchecked")
public class AracDaoImpl implements AracDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Arac> getByKeyword(String keyword) {
        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(Arac.class)
                .add(Restrictions.or(
                        Restrictions.ilike("marka", keyword, MatchMode.ANYWHERE),
                        Restrictions.ilike("model", keyword, MatchMode.ANYWHERE)
                ))
                .addOrder(Order.asc("marka"))
                .addOrder(Order.asc("model"));

        return criteria.list();

    }

    @Override
    public Arac getByPlaka(String plaka) {
        return (Arac) sessionFactory.getCurrentSession()
                .getNamedQuery(Arac.NQ.getByPlaka)
                .setParameter("plaka", plaka)
                .uniqueResult();
    }

    @Override
    public SorguSonucu getAracListesi(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        SorguSonucu ss = new SorguSonucu();

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Arac.class);
        //criteria.setFetchMode("dogumYeri", FetchMode.JOIN);
        if (!filters.keySet().isEmpty()) {
            for (String key : filters.keySet()) {
                criteria.add(Restrictions.ilike(key, (String) filters.get(key), MatchMode.ANYWHERE));
            }
        }
        criteria.setProjection(Projections.rowCount());
        ss.setRowCount(((Long) criteria.uniqueResult()).intValue());

        criteria.setProjection(null);
        if (sortField != null && sortField.equals("")) {
            if (sortOrder.equals(SortOrder.ASCENDING)) {
                criteria.addOrder(Order.asc(sortField));
            } else {
                criteria.addOrder(Order.desc(sortField));
            }
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setFirstResult(first);
        criteria.setFetchSize(pageSize);
        ss.setList(criteria.list());

        return ss;
    }

    @Override
    public Long filoAracSayisi() {
        return (Long) sessionFactory.getCurrentSession().createCriteria(Arac.class).setProjection(Projections.rowCount()).uniqueResult();
    }
}
