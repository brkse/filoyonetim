package com.burakkutbay.proje.rapor;

import com.burakkutbay.proje.arac.AracDao;
import com.burakkutbay.proje.il.IlDao;
import com.burakkutbay.proje.kurum.KurumDao;
import com.burakkutbay.proje.marka.MarkaDao;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.List;

/**
 * Created by hasanburakkutbay on 29.03.2017.
 */
@Controller("raporController")
@Scope("view")
@SuppressWarnings({"serial"})
public class RaporController implements Serializable {

    @Autowired
    private transient AracDao aracDao;

    @Autowired
    private transient KurumDao kurumDao;

    @Autowired
    private transient MarkaDao markaDao;

    @Autowired
    private transient IlDao ilDao;

    private transient String enCokBulunanAracBulunanKurum;

    private String aracFiloSayisi;
    private String kurumSayisi;
    private String enfazlamarka;

    private MapModel simpleModel;

    private List<Object[]> arabaBulunanIller;

    @PostConstruct
    public void init() {

        simpleModel = new DefaultMapModel();

        LatLng Adana = new LatLng(36.991419, 35.330829);
        LatLng Adiyaman = new LatLng(37.763650, 38.277259);
        LatLng Afyonkarahisar = new LatLng(38.756885, 30.538704);
        LatLng Agri = new LatLng(39.719074, 43.050591);
        LatLng Aksaray = new LatLng(38.368626, 34.0297);
        LatLng Amasya = new LatLng(40.656455, 35.837347);
        LatLng Ankara = new LatLng(39.933363, 32.859742);
        LatLng Antalya = new LatLng(41.1796748, 41.8093253);
        LatLng Ardahan = new LatLng(41.112950, 42.70228);
        LatLng Artvin = new LatLng(41.180937, 41.820819);
        LatLng Aydin = new LatLng(37.811703, 28.486396);
        LatLng Balikesir = new LatLng(39.653298, 27.890342);
        LatLng Bartin = new LatLng(41.626446, 32.336197);
        LatLng Batman = new LatLng(37.889517, 41.129283);
        LatLng Bayburt = new LatLng(40.260320, 40.228048);
        LatLng Bilecik = new LatLng(40.142573, 29.97933);
        LatLng Bingol = new LatLng(38.885464, 40.496625);
        LatLng Bitlis = new LatLng(38.398107, 42.111797);
        LatLng Bolu = new LatLng(40.732541, 31.608209);
        LatLng Burdur = new LatLng(37.718336, 30.282333);
        LatLng Bursa = new LatLng(40.188528, 29.060964);
        LatLng Canakkale = new LatLng(40.146720, 26.408587);
        LatLng Cankiri = new LatLng(40.600207, 33.616223);
        LatLng Corum = new LatLng(40.549926, 34.953724);
        LatLng Denizli = new LatLng(37.804223, 29.089050);
        LatLng Diyarbakir = new LatLng(37.924973, 40.210983);
        LatLng Duzce = new LatLng(40.838720, 31.162609);
        LatLng Edirne = new LatLng(41.677130, 26.555715);
        LatLng Elazig = new LatLng(38.674816, 39.222515);
        LatLng Gaziantep = new LatLng(37.065953, 37.37811);
        LatLng Giresun = new LatLng(40.917532, 38.392653);
        LatLng Gumushane = new LatLng(40.460825, 39.480321);
        LatLng Hakkari = new LatLng(37.557983, 43.750305);
        LatLng Hatay = new LatLng(36.209516, 36.194458);
        LatLng Isparta = new LatLng(37.762649, 30.553705);
        LatLng Mersin = new LatLng(36.812104, 34.641481);
        LatLng Istanbul = new LatLng(41.008238, 28.978359);
        LatLng Izmir = new LatLng(38.423734, 27.142826);
        LatLng Kars = new LatLng(40.601338, 43.097453);
        LatLng Kastamonu = new LatLng(41.677130, 26.555715);//Yapıldı
        LatLng Kayseri = new LatLng(38.720489, 35.482597);
        LatLng Kirklareli = new LatLng(41.735472, 27.224369);
        LatLng Kirsehir = new LatLng(39.146078, 34.159499);
        LatLng Kocaeli = new LatLng(40.775382, 29.948044);
        LatLng Konya = new LatLng(37.874643, 32.493155);
        LatLng Kutahya = new LatLng(39.419955, 29.985732);
        LatLng Malatya = new LatLng(38.351518, 38.335419);
        LatLng Manisa = new LatLng(38.614034, 27.429562);
        LatLng Kahramanmaras = new LatLng(37.575275, 36.922822);
        LatLng Mardin = new LatLng(37.344641, 40.731812);
        LatLng Mugla = new LatLng(37.214083, 28.364983);
        LatLng Muş = new LatLng(38.734561, 41.491038);
        LatLng Nevsehir = new LatLng(38.624694, 34.714151);
        LatLng Nigde = new LatLng(37.969774, 34.676608);
        LatLng Ordu = new LatLng(41.013713, 37.864380);
        LatLng Rize = new LatLng(41.025511, 40.517666);
        LatLng Sakarya = new LatLng(40.773074, 30.394817);
        LatLng Samsun = new LatLng(41.279703, 36.336067);
        LatLng Siirt = new LatLng(37.927404, 41.941978);
        LatLng Sinop = new LatLng(42.027974, 35.151725);
        LatLng Sivas = new LatLng(39.750545, 37.015022);
        LatLng Tekirdag = new LatLng(40.978092, 27.511674);
        LatLng Tokat = new LatLng(40.317559, 36.562500);
        LatLng Trabzon = new LatLng(41.000792, 39.715576);
        LatLng Tunceli = new LatLng(39.106170, 39.548259);
        LatLng Sanliurfa = new LatLng(37.240442, 38.891602);
        LatLng Usak = new LatLng(38.674229, 29.405882);
        LatLng Van = new LatLng(38.535947, 43.379517);
        LatLng Yozgat = new LatLng(39.821049, 34.808573);
        LatLng Zonguldak = new LatLng(41.453521, 31.78938);
        LatLng Karaman = new LatLng(37.181009, 33.222243);
        LatLng Sirnak = new LatLng(37.518974, 42.453714);
        LatLng Igdir = new LatLng(39.920060, 44.043615);
        LatLng Yalova = new LatLng(40.654895, 29.284186);
        LatLng Karabuk = new LatLng(41.195620, 32.622654);
        LatLng Kilis = new LatLng(36.716477, 37.114661);
        LatLng Osmaniye = new LatLng(37.074628, 36.2464);

        aracFiloSayisi = String.valueOf(aracDao.filoAracSayisi());
        kurumSayisi = String.valueOf(kurumDao.kurumSayisi());
        enCokBulunanAracBulunanKurum = kurumDao.enCokArabaBulunanKurum();
        enfazlamarka = markaDao.enCokBulunanArabaMarka();
        arabaBulunanIller = ilDao.arabaBulunanIller();

        for (int i = 0; i < arabaBulunanIller.size(); i++) {
            String il = String.valueOf(arabaBulunanIller.get(i));
            switch (il) {
                case "Adana":
                    simpleModel.addOverlay(new Marker(Adana, il));
                    break;
                case "Adıyaman":
                    simpleModel.addOverlay(new Marker(Adiyaman, il));
                    break;
                case "Afyonkarahisar":
                    simpleModel.addOverlay(new Marker(Afyonkarahisar, il));
                    break;
                case "Ağrı":
                    simpleModel.addOverlay(new Marker(Agri, il));
                    break;
                case "Aksaray":
                    simpleModel.addOverlay(new Marker(Aksaray, il));
                    break;
                case "Amasya":
                    simpleModel.addOverlay(new Marker(Amasya, il));
                    break;
                case "Ankara":
                    simpleModel.addOverlay(new Marker(Ankara, il));
                    break;
                case "Antalya":
                    simpleModel.addOverlay(new Marker(Antalya, il));
                    break;
                case "Ardahan":
                    simpleModel.addOverlay(new Marker(Ardahan, il));
                    break;
                case "Artvin":
                    simpleModel.addOverlay(new Marker(Artvin, il));
                    break;
                case "Aydın":
                    simpleModel.addOverlay(new Marker(Aydin, il));
                    break;
                case "Balıkesir":
                    simpleModel.addOverlay(new Marker(Balikesir, il));
                    break;
                case "Bartın":
                    simpleModel.addOverlay(new Marker(Bartin, il));
                    break;
                case "Batman":
                    simpleModel.addOverlay(new Marker(Batman, il));
                    break;
                case "Bayburt":
                    simpleModel.addOverlay(new Marker(Bayburt, il));
                    break;
                case "Bilecik":
                    simpleModel.addOverlay(new Marker(Bilecik, il));
                    break;
                case "Bingöl":
                    simpleModel.addOverlay(new Marker(Bingol, il));
                    break;
                case "Bitlis":
                    simpleModel.addOverlay(new Marker(Bitlis, il));
                    break;
                case "Bolu":
                    simpleModel.addOverlay(new Marker(Bolu, il));
                    break;
                case "Burdur":
                    simpleModel.addOverlay(new Marker(Burdur, il));
                    break;
                case "Bursa":
                    simpleModel.addOverlay(new Marker(Bursa, il));
                    break;
                case "Çanakkale":
                    simpleModel.addOverlay(new Marker(Canakkale, il));
                    break;
                case "Çankırı":
                    simpleModel.addOverlay(new Marker(Cankiri, il));
                    break;
                case "Çorum":
                    simpleModel.addOverlay(new Marker(Corum, il));
                    break;
                case "Denizli":
                    simpleModel.addOverlay(new Marker(Denizli, il));
                    break;
                case "Diyarbakır":
                    simpleModel.addOverlay(new Marker(Diyarbakir, il));
                    break;
                case "Düzce":
                    simpleModel.addOverlay(new Marker(Duzce, il));
                    break;
                case "Edirne":
                    simpleModel.addOverlay(new Marker(Edirne, il));
                    break;
                case "Elazığ":
                    simpleModel.addOverlay(new Marker(Elazig, il));
                    break;
                case "Gaziantep":
                    simpleModel.addOverlay(new Marker(Gaziantep, il));
                    break;
                case "Giresun":
                    simpleModel.addOverlay(new Marker(Giresun, il));
                    break;
                case "Gümüşhane":
                    simpleModel.addOverlay(new Marker(Gumushane, il));
                    break;
                case "Hakkari":
                    simpleModel.addOverlay(new Marker(Hakkari, il));
                    break;
                case "Hatay":
                    simpleModel.addOverlay(new Marker(Hatay, il));
                    break;
                case "Isparta":
                    simpleModel.addOverlay(new Marker(Isparta, il));
                    break;
                case "Mersin":
                    simpleModel.addOverlay(new Marker(Mersin, il));
                    break;
                case "İstanbul":
                    simpleModel.addOverlay(new Marker(Istanbul, il));
                    break;
                case "İzmir":
                    simpleModel.addOverlay(new Marker(Izmir, il));
                    break;
                case "Kars":
                    simpleModel.addOverlay(new Marker(Kars, il));
                    break;
                case "Kastamonu":
                    simpleModel.addOverlay(new Marker(Kastamonu, il));
                    break;
                case "Kayseri":
                    simpleModel.addOverlay(new Marker(Kayseri, il));
                    break;
                case "Kırklareli":
                    simpleModel.addOverlay(new Marker(Kirklareli, il));
                    break;
                case "Kırşehir":
                    simpleModel.addOverlay(new Marker(Kirsehir, il));
                    break;
                case "Kocaeli":
                    simpleModel.addOverlay(new Marker(Kocaeli, il));
                    break;
                case "Konya":
                    simpleModel.addOverlay(new Marker(Konya, il));
                    break;
                case "Kütahya":
                    simpleModel.addOverlay(new Marker(Kutahya, il));
                    break;
                case "Malatya":
                    simpleModel.addOverlay(new Marker(Malatya, il));
                    break;
                case "Manisa":
                    simpleModel.addOverlay(new Marker(Manisa, il));
                    break;
                case "Kahramanmaraş":
                    simpleModel.addOverlay(new Marker(Kahramanmaras, il));
                    break;
                case "Mardin":
                    simpleModel.addOverlay(new Marker(Mardin, il));
                    break;
                case "Muğla":
                    simpleModel.addOverlay(new Marker(Mugla, il));
                    break;
                case "Muş":
                    simpleModel.addOverlay(new Marker(Muş, il));
                    break;
                case "Nevşehir":
                    simpleModel.addOverlay(new Marker(Nevsehir, il));
                    break;
                case "Niğde":
                    simpleModel.addOverlay(new Marker(Nigde, il));
                    break;
                case "Ordu":
                    simpleModel.addOverlay(new Marker(Ordu, il));
                    break;
                case "Rize":
                    simpleModel.addOverlay(new Marker(Rize, il));
                    break;
                case "Sakarya":
                    simpleModel.addOverlay(new Marker(Sakarya, il));
                    break;
                case "Samsun":
                    simpleModel.addOverlay(new Marker(Samsun, il));
                    break;
                case "Siirt":
                    simpleModel.addOverlay(new Marker(Siirt, il));
                    break;
                case "Sinop":
                    simpleModel.addOverlay(new Marker(Sinop, il));
                    break;
                case "Sivas":
                    simpleModel.addOverlay(new Marker(Sivas, il));
                    break;
                case "Tekirdağ":
                    simpleModel.addOverlay(new Marker(Tekirdag, il));
                    break;
                case "Tokat":
                    simpleModel.addOverlay(new Marker(Tokat, il));
                    break;
                case "Trabzon":
                    simpleModel.addOverlay(new Marker(Trabzon, il));
                    break;
                case "Tunceli":
                    simpleModel.addOverlay(new Marker(Tunceli, il));
                    break;
                case "Şanlıurfa":
                    simpleModel.addOverlay(new Marker(Sanliurfa, il));
                    break;
                case "Uşak":
                    simpleModel.addOverlay(new Marker(Usak, il));
                    break;
                case "Van":
                    simpleModel.addOverlay(new Marker(Van, il));
                    break;
                case "Yozgat":
                    simpleModel.addOverlay(new Marker(Yozgat, il));
                    break;
                case "Zonguldak":
                    simpleModel.addOverlay(new Marker(Zonguldak, il));
                    break;
                case "Karaman":
                    simpleModel.addOverlay(new Marker(Karaman, il));
                    break;
                case "Şırnak":
                    simpleModel.addOverlay(new Marker(Sirnak, il));
                    break;
                case "Iğdır":
                    simpleModel.addOverlay(new Marker(Igdir, il));
                    break;
                case "Yalova":
                    simpleModel.addOverlay(new Marker(Yalova, il));
                    break;
                case "Karabük":
                    simpleModel.addOverlay(new Marker(Karabuk, il));
                    break;
                case "Kilis":
                    simpleModel.addOverlay(new Marker(Kilis, il));
                    break;
                case "Osmaniye":
                    simpleModel.addOverlay(new Marker(Osmaniye, il));
                    break;
            }
        }
    }

    public String getAracFiloSayisi() {
        return aracFiloSayisi;
    }

    public void setAracFiloSayisi(String aracFiloSayisi) {
        this.aracFiloSayisi = aracFiloSayisi;
    }

    public String getKurumSayisi() {
        return kurumSayisi;
    }

    public void setKurumSayisi(String kurumSayisi) {
        this.kurumSayisi = kurumSayisi;
    }

    public String getEnCokBulunanAracBulunanKurum() {
        return enCokBulunanAracBulunanKurum;
    }

    public void setEnCokBulunanAracBulunanKurum(String enCokBulunanAracBulunanKurum) {
        this.enCokBulunanAracBulunanKurum = enCokBulunanAracBulunanKurum;
    }

    public String getEnfazlamarka() {
        return enfazlamarka;
    }

    public void setEnfazlamarka(String enfazlamarka) {
        this.enfazlamarka = enfazlamarka;
    }

    public MapModel getSimpleModel() {
        return simpleModel;
    }

}
