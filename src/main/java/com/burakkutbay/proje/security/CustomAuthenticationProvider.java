package com.burakkutbay.proje.security;


import com.burakkutbay.proje.kullanici.Kullanici;
import com.burakkutbay.proje.kullanici.KullaniciDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.util.StringUtils;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */
public class CustomAuthenticationProvider implements AuthenticationProvider {


    @Autowired
    private KullaniciDao kullaniciDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String kullaniciAdi = String.valueOf(authentication.getPrincipal());

        if (!StringUtils.isEmpty(kullaniciAdi)) {
            Kullanici kullanici = kullaniciDao.getByKullaniciAdi(kullaniciAdi);

            if (kullanici != null) {
                String parola = String.valueOf(authentication.getCredentials());

                if (passwordEncoder.encodePassword(parola, null).equals(kullanici.getParola()) && kullanici.isAktif()) {

                    GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();
                    UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
                            authentication.getPrincipal(),
                            authentication.getCredentials(),
                            authoritiesMapper.mapAuthorities(kullanici.getAuthorities()));

                    result.setDetails(authentication.getDetails());
                    return result;

                }
            }
            return null;
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
