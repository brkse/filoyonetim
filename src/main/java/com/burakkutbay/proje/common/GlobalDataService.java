package com.burakkutbay.proje.common;

import com.burakkutbay.freamework.GenericDao;
import com.burakkutbay.proje.il.Il;
import com.burakkutbay.proje.marka.Marka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by hasanburakkutbay on 23.03.2017.
 */

@Service("globalDataService")
@Scope("singleton")
@SuppressWarnings("unchecked")
public class GlobalDataService {

    @Autowired
    private GenericDao genericDao;

    private String aktifstye;

    @PostConstruct
    public void init() {

        refreshIlListesi();
        refreshMarkaListesi();
    }

    private List<Il> ilListesi;

    private List<Marka> markaListesi;

    public List<Il> getIlListesi() {
        return ilListesi;
    }

    public List<Marka> getMarkaListesi() {
        return markaListesi;
    }

    public void refreshIlListesi() {
        this.ilListesi = genericDao.getAll(Il.class);
    }

    public void refreshMarkaListesi() {
        this.markaListesi = genericDao.getAll(Marka.class);
    }

    public String getAktifstye() {
        return aktifstye;
    }

    public void setAktifstye(String aktifstye) {
        this.aktifstye = aktifstye;
    }


}
